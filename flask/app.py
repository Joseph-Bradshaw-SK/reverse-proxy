from flask import Flask
from flask_socketio import SocketIO
from socket import gethostname 

app = Flask(__name__)
socketio = SocketIO(app, logger=True, engineio_logger=True)


@app.route("/")
def hello_world():
    return f'Host: {gethostname()}'


@socketio.on('message')
def handle_message(data):
    print(f'received message: {data}')


if __name__ == '__main__':
    socketio.run(app, debug=True, host='0.0.0.0')

