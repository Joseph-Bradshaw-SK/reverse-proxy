from sys import exit
import asyncio
from aiohttp import web, WSCloseCode, WSMsgType
from socket import gethostname


async def http_handler(request):
    return web.Response(text=f"Host: {gethostname()}")


async def websocket_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        print(f'Received: {msg}')
        if msg.type == WSMsgType.TEXT:
            if msg.data == 'close':
                print('closing connection..')
                await ws.close()
                print('connection closed.')
                exit()
            else:
                print('echoing output back to sender')
                await ws.send_str('some websocket message payload')
        elif msg.type == WSMsgType.ERROR:
            print(f'ws connection closed with exception: {ws.exception()}')
    return ws


def create_runner():
    app = web.Application()
    app.add_routes([
        web.get('/', http_handler),
        web.get('/ws', websocket_handler)
        ])
    return web.AppRunner(app)


async def start_server(host="0.0.0.0", port="5000"):
    runner = create_runner()
    await runner.setup()
    site = web.TCPSite(runner, host, int(port))
    await site.start()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_server())
    loop.run_forever()


