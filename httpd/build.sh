#!/usr/bin/bash
set -e
build=$(buildah from registry.fedoraproject.org/fedora-minimal:35)
build_mount=$(buildah mount ${build})

# Install software and copy configuration file over for Apache
buildah run ${build} -- microdnf install -y httpd mod_proxy_html

# Install editor for help debugging
buildah run ${build} -- microdnf install -y vim

cp ./httpd.conf ${build_mount}/etc/httpd/conf/httpd.conf

# Cleanup cache files to minimize file size
rm -rf  ${build_mount}/var/cache/* || true
rm -rf ${build_mount}/var/log/*  || true
rm -rf ${build_mount}/tmp/*  || true


# Create the required directory for logs
mkdir -p ${build_mount}/var/log/httpd
chown apache:apache ${build_mount}/var/log/httpd
buildah config --entrypoint '[ "httpd" ]' --cmd "-DFOREGROUND" ${build}

# Save image locally
buildah commit ${build} localhost/httpd-proxy
