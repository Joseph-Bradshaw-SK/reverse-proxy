from time import sleep
import numpy as np
import os
from shyft.time_series import (point_interpretation_policy as point_fx, TimeAxis, TimeSeries, TsVector, time, Calendar, deltahours,DtsClient,DtsServer,shyft_url,utctime_now)


class DtsService():
  def __init__(self):
    self.s: DtsServer = DtsServer()
    self.s.set_auto_cache(True)
    self.s.set_container("test", os.getcwd() + "/data")
    print(f'container at: test/{os.getcwd()}/data')
    self.c: DtsClient = None

  def __enter__(self):
    port_no = self.s.start_async()
    print(f"Setting up web API service., raw api at {port_no}")
    self.c = DtsClient(rf"localhost:{port_no}")
    self.s.start_web_api('0.0.0.0', 30102, os.getcwd() + '/')
    print(f'web api at: 0.0.0.0:30102/ in {os.getcwd()}/')
    return self.c

  def __exit__(self, type, value, traceback):
    self.c.close()
    self.s.stop_web_api()
    self.s.clear()
    del self.c
    del self.s


def shyft_store_url(name: str) -> str:
  return shyft_url("test", name)


def make_ts_fragment(*, x:float,noise:float, ta:TimeAxis)->TimeSeries:
  t=ta.time_points_double[:-1]
  v= np.sin(t*x)+np.random.random([ta.size()])*noise
  return TimeSeries(ta,v,point_fx.POINT_AVERAGE_VALUE)


def store_some_time_series(*, client: DtsClient, ta: TimeAxis):
  n_ts = 20
  tsv = TsVector()  # something we store at server side
  for i in range(n_ts):
    tsv.append(TimeSeries(shyft_store_url(f"ts{i}"),make_ts_fragment(x=3.14/(3600.0*12+i), noise=0.15,ta=ta)))  # generate a bound pts to store
  client.store_ts(tsv=tsv, overwrite_on_write=False, cache_on_write=True)


if __name__ == '__main__':
  with DtsService() as client:
    ta = TimeAxis(time('2020-01-01T00:00:00Z'),time(3600), 24*10)
    store_some_time_series(client=client,ta=ta)
    # Let's verify the results:
    print(os.listdir('.'))
    print(os.listdir('./data/'))
    while True:
        # Stop the fans from going crazy :)
        sleep(0.2)
