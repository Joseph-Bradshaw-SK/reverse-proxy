#!/usr/bin/bash
set -e
build=$(buildah from registry.gitlab.com/statkraft/adam/containers/adam-x-bbox)
build_mount=$(buildah mount ${build})

# mkdir -p ${build_mount}/var/www/html
cp ./index.html ${build_mount}/index.html
cp ./make_ts_and_host.py ${build_mount}/make_ts_and_host.py
       
# Cleanup cache files to minimize file size
rm -rf  ${build_mount}/var/cache/* || true
rm -rf ${build_mount}/var/log/*  || true
rm -rf ${build_mount}/tmp/*  || true

buildah config --entrypoint '[ "python" ]' --cmd "make_ts_and_host.py" ${build}

buildah commit ${build} localhost/dtss-service
